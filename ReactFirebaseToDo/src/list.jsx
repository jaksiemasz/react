var React = require('react');
var ListItem = require('./list-item');

var List = React.createClass({
	render: function () {
		return <ul>{this.renderList()}</ul>
	},
	renderList: function () {
		console.log(Object.keys(this.props.items).length)
		if(this.props.items && Object.keys(this.props.items).length ===0){
			return <h4> 
				Add item to your todo list
			</h4>
		}else{
			var children = [];
			for(var key in this.props.items) {
				var item = this.props.items[key];
				item.key = key;
				children.push(
					<ListItem item={item} key={key}>
					</ListItem>
				)	
			}
			return children;
		}

	}
});

module.exports = List;